package net.supraball

import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import net.supraball.json.Server
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplicationTest {

    @Test
    fun testList() {
        withTestApplication({ module(testing = true) }) {
            // Add a server to list
            ServerManager.update(
                Server(
                    null, "Title", "Description", "127.0.0.1",7777, 6,
                    10, 5, 15, 10,
                    1, 1, 6, 2,
                    3, "db-hall", 27088, "1.0.0",
                    false, false, 1, emptyArray()
                )
            )

            handleRequest(HttpMethod.Post, "/list").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                println(response.content)
                assertEquals(
                    "{\"servers\":[{\"ip\":\"127.0.0.1\",\"port\":7777," +
                            "\"title\":\"Title\",\"description\":\"Description\",\"max_players\":6," +
                            "\"max_spectators\":10,\"max_duration\":15,\"max_goals\":10," +
                            "\"current_players\":1,\"current_spectators\":1,\"current_duration\":6," +
                            "\"current_goals_red\":2,\"current_goals_blue\":3,\"map\":\"db-hall\"," +
                            "\"training\":false}]}",
                    response.content
                )
            }
        }
    }
}
