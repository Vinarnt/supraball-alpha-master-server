package net.supraball

import com.google.gson.FieldNamingPolicy
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.origin
import io.ktor.gson.gson
import io.ktor.request.header
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import net.supraball.json.*
import net.supraball.json.adapter.UpdateRequestTypeAdapter
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate


fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)

    // Cleanup routine
    Timer().scheduleAtFixedRate(10000L, 10000L) {
        ServerManager.cleanup()
    }
}

@Suppress("unused")
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(ContentNegotiation) {
        gson {
            setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            registerTypeAdapter(UpdateRequest::class.java, UpdateRequestTypeAdapter())
        }
    }

    routing {
        // Testing route
        get("/") {
            call.respond("{success: true}")
        }

        // Get the list of registered servers
        post("/list") {
            val servers = ServerManager.getAll()
                .map { server: Server ->
                    with(server) {
                        ServerEntry(
                            ip!!,
                            port,
                            title,
                            description,
                            maxPlayers,
                            maxSpectators,
                            maxDuration,
                            maxGoals,
                            currentPlayers,
                            currentSpectators,
                            currentDuration,
                            currentGoalsRed,
                            currentGoalsBlue,
                            map,
                            training
                        )
                    }
                }

            call.respond(ServerList(servers))
        }

        // Register or update a registered server
        post("/update") {
            val request = call.receive(UpdateRequest::class)

            // Check IP
            if (request.server.ip == null) {
                // Check if it contains a forwarded header
                val providedHost: String? = request.server.ip
                val remoteHost: String = call.request.origin.remoteHost
                val forwardedHost: String? = call.request.header("X-Forwarded-For")
                when {
                    providedHost != null -> providedHost
                    forwardedHost != null -> forwardedHost
                    else -> remoteHost
                }.also { request.server.ip = it }
            }

            val server = ServerManager.update(request.server)

            call.respond(
                UpdateResponse(server.ip!!, 15, server.id ?: "", server.maxPlayers != 6)
            )
        }
    }
}
