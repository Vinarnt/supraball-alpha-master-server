package net.supraball

import net.supraball.json.Server
import org.slf4j.Logger
import org.slf4j.LoggerFactory


object ServerManager {

    private val LOGGER: Logger = LoggerFactory.getLogger("ServerManager")

    private const val CLEANUP_TIMEOUT_DELAY = 15L * 1000L

    private var servers: MutableMap<String, Server> = mutableMapOf()
    private var currentId: Long = 1

    fun update(server: Server): Server {

        val isUpdate = server.id != null && servers.containsKey(server.id!!)
        val oldServer: Server = if (!isUpdate) {
            currentId.toString().let { id ->
                server.id = id
                currentId++
                servers[server.id!!] = server

                server

            }
        } else {
            servers[server.id]!!
        }

        oldServer.apply {
            this.currentPlayers = server.currentPlayers
            this.currentSpectators = server.currentSpectators
            this.currentDuration = server.currentDuration
            this.currentGoalsRed = server.currentGoalsRed
            this.currentGoalsBlue = server.currentGoalsBlue
            this.players = server.players
            this.lastUpdate = System.currentTimeMillis()
        }

        when (isUpdate) {
            true -> LOGGER.debug("Updated server {}({})", oldServer.title, oldServer.ip)
            else -> LOGGER.info("Added server {}({})", oldServer.title, oldServer.ip)
        }

        return oldServer
    }

    fun getAll(): MutableCollection<Server> = servers.values

    fun cleanup() {
        servers.iterator().let {
            while (it.hasNext()) {
                val (_, value) = it.next()
                if ((System.currentTimeMillis() - value.lastUpdate) >= CLEANUP_TIMEOUT_DELAY) {
                    it.remove()
                    LOGGER.info("Removed server {}({}) after cleanup", value.title, value.ip)
                }
            }
        }
    }
}