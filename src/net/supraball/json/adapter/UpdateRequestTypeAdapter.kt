package net.supraball.json.adapter

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import net.supraball.json.Server
import net.supraball.json.UpdateRequest


class UpdateRequestTypeAdapter : TypeAdapter<UpdateRequest>() {

    override fun read(reader: JsonReader): UpdateRequest {

        val properties: MutableMap<String, Any> = mutableMapOf()
        reader.beginObject()

        var fieldname = ""
        while (reader.hasNext()) {
            var token: JsonToken = reader.peek()

            if (token == JsonToken.NAME) {
                fieldname = reader.nextName()
            }

            when (fieldname) {
                "id", "title", "description", "ip", "map", "ver"
                -> reader.nextString()

                "port", "max_players", "max_goals", "max_goal_difference", "max_duration",
                "max_spectators", "current_players", "current_spectators", "current_duration",
                "current_goals_red", "current_goals_blue", "query_port", "tie_breaker"
                -> reader.nextInt()

                "passworded", "training"
                -> reader.nextBoolean()

                "players" -> {
                    Gson().fromJson(reader, Array<String>::class.java)
                }
                else -> throw IllegalStateException("Field $fieldname not expected")
            }.let { it?.let { value -> properties[fieldname] = value } }
                .also { token = reader.peek() }
        }

        reader.endObject()

        return UpdateRequest(
            Server(
                properties["id"]?.toString(),
                properties["title"].toString(),
                properties["description"].toString(),
                properties["ip"]?.toString(),
                properties["port"] as Int,
                properties["max_players"] as Int,
                properties["max_goals"] as Int,
                properties["max_goal_difference"] as Int,
                properties["max_duration"] as Int,
                properties["max_spectators"] as Int,
                properties["current_players"] as Int,
                properties["current_spectators"] as Int,
                properties["current_duration"] as Int,
                properties["current_goals_red"] as Int,
                properties["current_goals_blue"] as Int,
                properties["map"].toString(),
                properties["query_port"] as Int,
                properties["ver"].toString(),
                properties["passworded"] as Boolean,
                properties["training"] as Boolean,
                properties["tie_breaker"] as Int,
                properties["players"] as Array<String>
            )
        )
    }

    override fun write(writer: JsonWriter, value: UpdateRequest) {
    }
}