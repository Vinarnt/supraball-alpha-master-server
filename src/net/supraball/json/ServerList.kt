package net.supraball.json


data class ServerList(
    val servers: List<ServerEntry>?
)