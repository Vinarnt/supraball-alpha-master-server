package net.supraball.json


data class Server(
    var id: String?,
    val title: String,
    val description: String,
    var ip: String?,
    val port: Int,
    val maxPlayers: Int,
    val maxGoals: Int,
    val maxGoalDifference: Int,
    val maxDuration: Int,
    val maxSpectators: Int,
    var currentPlayers: Int,
    var currentSpectators: Int,
    var currentDuration: Int,
    var currentGoalsRed: Int,
    var currentGoalsBlue: Int,
    var map: String,
    val queryPort: Int,
    val ver: String,
    val passworded: Boolean,
    val training: Boolean,
    val tieBreaker: Int,
    var players: Array<String>,
    var lastUpdate: Long = System.currentTimeMillis()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Server

        if (id != other.id) return false
        if (title != other.title) return false
        if (description != other.description) return false
        if (ip != other.ip) return false
        if (port != other.port) return false
        if (maxPlayers != other.maxPlayers) return false
        if (maxGoals != other.maxGoals) return false
        if (maxGoalDifference != other.maxGoalDifference) return false
        if (maxDuration != other.maxDuration) return false
        if (maxSpectators != other.maxSpectators) return false
        if (currentPlayers != other.currentPlayers) return false
        if (currentSpectators != other.currentSpectators) return false
        if (currentDuration != other.currentDuration) return false
        if (currentGoalsRed != other.currentGoalsRed) return false
        if (currentGoalsBlue != other.currentGoalsBlue) return false
        if (map != other.map) return false
        if (queryPort != other.queryPort) return false
        if (ver != other.ver) return false
        if (passworded != other.passworded) return false
        if (training != other.training) return false
        if (tieBreaker != other.tieBreaker) return false
        if (!players.contentEquals(other.players)) return false
        if (lastUpdate != other.lastUpdate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + title.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + ip.hashCode()
        result = 31 * result + port
        result = 31 * result + maxPlayers
        result = 31 * result + maxGoals
        result = 31 * result + maxGoalDifference
        result = 31 * result + maxDuration
        result = 31 * result + maxSpectators
        result = 31 * result + currentPlayers
        result = 31 * result + currentSpectators
        result = 31 * result + currentDuration
        result = 31 * result + currentGoalsRed
        result = 31 * result + currentGoalsBlue
        result = 31 * result + map.hashCode()
        result = 31 * result + queryPort
        result = 31 * result + ver.hashCode()
        result = 31 * result + passworded.hashCode()
        result = 31 * result + training.hashCode()
        result = 31 * result + tieBreaker
        result = 31 * result + players.contentHashCode()
        result = 31 * result + lastUpdate.hashCode()
        return result
    }
}