package net.supraball.json


data class UpdateResponse(val ip: String, val timeout: Short, val id: String, val is3v3: Boolean)