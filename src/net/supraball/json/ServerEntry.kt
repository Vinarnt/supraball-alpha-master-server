package net.supraball.json


data class ServerEntry(
    val ip: String,
    val port: Int,
    val title: String,
    val description: String,
    val maxPlayers: Int,
    val maxSpectators: Int,
    val maxDuration: Int,
    val maxGoals: Int,
    val currentPlayers: Int,
    val currentSpectators: Int,
    val currentDuration: Int,
    val currentGoalsRed: Int,
    val currentGoalsBlue: Int,
    val map: String,
    val training: Boolean
)