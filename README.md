# Supraball Alpha Master Server

## Build
#### For a zip production ready
```shell
gradlew distZip
```
Result will be in `build/distributions`

#### For a folder production ready
```shell
gradlew isntallDist
```
Result will be in `build/install`

#### To run it from sources
```shell
gradlew run
```